#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 21:31:14 2020

@author: andria
"""

i = 4
d = 4.0
s = 'HackerRank '
# Declare second integer, double, and String variables.
i2, d2, s2 = 0, 0.0, ''
# Read and save an integer, double, and String to your variables.
i2 = int(input())
d2 = float(input())
s2 = str(input())

# Print the sum of both integer variables on a new line.
print(i + i2)
# Print the sum of the double variables on a new line.
print(d + d2)
# Concatenate and print the String variables on a new line
# The 's' variable above should be printed first.
print(s + s2)