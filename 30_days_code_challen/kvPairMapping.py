#!/bin/python3

import math
import os
import random
import re
import sys

n = int(input('Enter an integer: '))
if (n < 1 and n > 100000):
    n = int(input('Reenter an integer: '))
else:
    phoneBook = {}
    for i in range(n):
        s = str(input('Enter a name and phone: '))
        name, phoneNumber = s.split(' ', 1)
        # print(name, phoneNumber)
        phoneBook[name] = phoneNumber
    # print(phoneBook)
    try:
        while True:
            query = str(input('Enter a name: '))
            if query != "":
                if query in phoneBook.keys():
                    print('{}={}'.format(query, phoneBook[query]))
                else:
                    print('Not found')
            else:
                break
    except EOFError:
	    pass
    