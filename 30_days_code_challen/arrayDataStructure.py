#!/bin/python3

import math
import os
import random
import re
import sys


n = int(input('Enter an integer: '))
if (n < 1 and n > 1000):
    n = int(input('Reenter an integer: '))
else:
    arr = list(map(int, input().rstrip().split()))
    if len(arr) != n:
        print('Something went wrong!')
    else:
        arr_inv = ''
        for i in range(1, len(arr)):
            arr_inv += str(arr[-i])
            arr_inv += ' '
        print(arr_inv + '' + str(arr[0]))
