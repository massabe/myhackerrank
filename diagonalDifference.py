#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 15:40:59 2020

@author: andria
"""

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

def diagonalDifference(arr):
    # Write your code here
    diag1 = 0
    diag2 = 0
    for i in range(len(arr)):
        diag1 += arr[i][i] 
    for j in range(len(arr)):
        arr2 = arr[::-1]
        diag2 += arr2[j][j]
        print(diag2)
       
    result = abs(diag1 - diag2)
    return result
            
    


n = 3

arr = [[11, 2, 4],
       [4, 5, 6],
       [10, 8, -12]
       ]
result = diagonalDifference(arr)
print(result)

    
