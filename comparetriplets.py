#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 12:04:01 2020

@author: andria
"""

import math
import os
import random
import re
import sys

# Complete the compareTriplets function below.
def compareTriplets(a, b):
    res = [0,0]
    for i in range(3):
        if a[i] > b[i]:
            res[0] += 1
        elif a[i] == b[i]:
            pass
        else:
            res[1] += 1
    
    return res        
        
        


a = [5, 6, 7]

b = [3, 6, 10]

result = compareTriplets(a, b)

print(result)