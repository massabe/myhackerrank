#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 21:21:52 2020

@author: andria
"""

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solve(meal_cost, tip_percent, tax_percent):
    tip = meal_cost * tip_percent/100
    tax = meal_cost * tax_percent/100
    totalCost = meal_cost + tip + tax
    return round(totalCost)
meal_cost = 12
tip_percent = 20
tax_percent = 8

totalCost = solve(meal_cost, tip_percent, tax_percent)
print(totalCost)