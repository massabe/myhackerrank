#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 16:55:21 2020

@author: andria
"""

import math
import os
import random
import re
import sys

# Complete the staircase function below.
def staircase(n):
    for i in range(1, n+1):
        print( ' '* (n-i) + i* '#')

n = 6
staircase(n)
