#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 16:34:27 2020

@author: andria
"""

import math
import os
import random
import re
import sys

# Complete the plusMinus function below.
def plusMinus(arr):
    pos,  neg, z = 0, 0, 0
    for i in arr:
        if i > 0:
            pos += 1
        elif i < 0:
            neg += 1
        else:
            z += 1
    r_pos = pos / len(arr)
    r_neg = neg / len(arr)
    r_z = z / len(arr)
    print('%.6f' % r_pos)
    print('%.6f' % r_neg)
    print('%.6f' % r_z)
    



arr = [-4, 3, -9, 0, 4, 1]
plusMinus(arr)

