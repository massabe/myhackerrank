#!/bin/python3

import math
import os
import random
import re
import sys

n = int(input('Enter an integer: '))
if (n < 1 and n > 1000000):
    n = int(input('Reenter an integer: '))
else:
    b = bin(n)[2:] + '0'
    print(b)
    
    counts = []
    count = 1
    for a, b in zip(b, b[1:]):
        if a==b:
            count += 1
        else:
            counts.append((a, count))
            count = 1
    num = []
    for c in range(len(counts)):
        if counts[c][0] == '1':
            num.append(counts[c][1])
    print(max(num))


