#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 12:46:42 2020

@author: andria
"""

import math
import os
import random
import re
import sys

# Complete the aVeryBigSum function below.
def aVeryBigSum(ar):
    result = ar[0]
    for i in range(1,len(ar)):
        result += ar[i]
    return result
        
    



ar_count = 5

ar = [1000000001, 1000000002, 1000000003, 1000000004, 1000000005]

result = aVeryBigSum(ar)
print(result)